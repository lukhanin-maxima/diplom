import Cookies from 'universal-cookie';
import styled from "styled-components";
import {useEffect, useState} from "react";
import { Route, Routes, BrowserRouter } from 'react-router-dom';

import  Header  from  "./components/header/header.js";
import Library from "./components/library/library.js";
import Author from "./components/info/author.js";
import BodyContainer from "./components/body/bodyContainer";

const ModalWindow = styled.div`
  position: absolute;
  background: #2FA8F2;
  border: solid black 1px;
  padding: 10px;
  border-radius: 15px;
  top: 25%;
  left: 40%;
  z-index:99999;

  .input {
    border-radius: 5px;
    border: 0;
    height: 20px;
    width: 100%;
  }

  .error {
    color: red;
    font-weight: bold;
  }
`
const ModalWrappep = styled.div`
  position: absolute;
  backdrop-filter: blur(10px);
  width: 100%;
  height: 100%;
`
const Button = styled.button`
  margin-top: 15px;
  width: 105px;
  height: 40px;
  border-radius: 15px;
  border: 0;
  background: #33cccc;`;

function App() {
    const cookies = new Cookies();

    const [error, setError] = useState('')
    const [modalOn, setModalOn] = useState(false);
    const [login, setLogin] = useState('login');
    const [password, setPassword] = useState('');
    const [logged, setLogged] = useState(false);
    useEffect(()=>{
        if(cookies.get('converterLoggin')){
            setModalOn(false);
            setLogged(true);
        }
    },[cookies])
    useEffect(() => {
        if (logged) {
            setModalOn(false);
        }
        if (!logged) {
            setModalOn(true);
        }
    }, [logged]);

    function signUp() {
        if (login === 'currency' && password === 'converter') {
            cookies.set('converterLoggin', true);
            setLogged(true);
        } else {
            setError('Неверный логин или пароль')
        }
    }

    return (
        <div>
            {modalOn ? (
                <ModalWrappep>
                    <ModalWindow>
                        <div>Логин</div>
                        <input value={login} onChange={(e) => setLogin(e.target.value)} className='input'/>
                        <div>Пароль</div>
                        <input type='password' onChange={(event) => setPassword(event.target.value)}
                               className='input'></input>
                        {error !== '' && (
                            <div className='error'>{error}</div>)
                        }
                        <div onClick={() => signUp()}>
                            <Button>Войти</Button>
                        </div>
                    </ModalWindow>
                </ModalWrappep>
            ):


            <BrowserRouter>
<Header setLogged={setLogged}/>

                <Routes>
                    <Route path='/' element={<BodyContainer logged={logged}/>}/>
                    <Route path='library' element={<Library />}/>
                    <Route path='author' element={<Author />}/>
                </Routes>
            </BrowserRouter>}


        </div>
    );
}

export default App;
