const SET_VALUE = 'setValue'
const SET_RUB = 'setRub'

let initialState = {
    CAD: 0,
    EUR: 0,
    USD:0,
    RUB:0,
}

const converterRecuder = (state = initialState,action) =>{
    switch (action.type){
        case SET_VALUE:{
            let copyState = { ...state };
            copyState.CAD = action.data.CAD;
            copyState.EUR = action.data.EUR;
            copyState.USD = action.data.USD;
            return copyState;
        }
        case SET_RUB:{
            let copyState = {...state};
            copyState.RUB = parseInt(action.data);
            return copyState;

        }
        default:
            return state;
    }

}

export const setValueData = (data) => ({ type: SET_VALUE, data });
export const setRubData = (data) => ({ type: SET_RUB, data });

export default converterRecuder;