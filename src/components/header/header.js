
 import './header.css';
import styled from "styled-components";
import Cookies from "universal-cookie";
import {NavLink} from "react-router-dom";




const HeaderWrapper = styled.div`
  
  background: #2FA8F2;
  height: 50px;
  color: white;
  font-weight: bold;
  padding: 5px;
  .logout{
    text-align: right;
    cursor: pointer;
  }
  .input{
    border-radius: 5px;
    border: 0;
    height: 20px;
    width: 10%;
  }
`
function logout(){
    const cookies = new Cookies();
    cookies.remove('converterLoggin');
}

function Header(props) {


    return (
        <div>
            <HeaderWrapper>
              <div className='header-container'>
             <div className='logout' onClick={()=>{
                 logout();
                 props.setLogged(false);
             }}>Выйти</div>



                <NavLink to='author'>Об авторе</NavLink>
                <NavLink to='library'>Библиотека рецептов</NavLink>
                <NavLink to='/'>Конвертер валют</NavLink>
                </div>
            </HeaderWrapper>
        </div>
    );
}

export default Header;
