import React from "react";
import './recipe.css'


const Recipe = ({title, calories, image, ingredients}) => {
    return(
        <div className='recipe'>
            <h1>{title}</h1>
            <ul>
                {ingredients.map((ingredients, index) => (
                    <li key={index} >{ingredients.text}</li>
                    ))}
            </ul>
            <p>{Math.round(calories)} calories</p>
            <img className='image' src={image} alt=""/>
        </div>
    );
}

export default Recipe;