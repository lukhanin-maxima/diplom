import { connect } from 'react-redux';
import Body from './body';
import {setRubData, setValueData} from "../../reducers/consverter";

let mapStateToProps = (state) => {
    return {
        converterReducer: state.converterReducer,
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        setValue: (data) => {
            dispatch(setValueData(data));
        },
        setRubData: (data) => {
            dispatch(setRubData(data));
        },
    };
};
const bodyContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Body);
export default bodyContainer;
