import './body.css';
import styled from "styled-components";
import axios from "axios";
import {useEffect } from "react";



const BodyWrapper = styled.div`
  padding: 25px;

  .imgStyle {
    border-radius: 50%;
  }

  .element {
    display: flex;
    align-items: center;
    height: 30px;
  }
`


function Body(props) {

    const api = axios.create({
        baseURL: 'https://api.freecurrencyapi.com/v1/latest?apikey=CRTAyTL0eQVGbhnQuQHmPa2RLCKwN0OHstfrVtpg&currencies=EUR%2CUSD%2CCAD&base_currency=RUB',
        responseType: 'json',
        withCredentials: true,

    });


    useEffect(() => {
        if (props.logged) {
            api.get('')
                .then((event) => {
                    props.setValue(event.data.data);
                });
        }

    }, [props.logged]);


    return (
        <div className="calculator">
            <BodyWrapper>
                <input type='number' placeholder="Введите сумму ₽" onChange={(e) => {
                    props.setRubData(e.target.value);
                }}/>

                    <div>
                        <div className='element'>
                            <img
                                alt='cad pic'
                                className='imgStyle'
                                width='50px'
                                src='/img/cad.jpg'
                            />
                            CAD: {props.converterReducer.CAD} = {props.converterReducer.RUB * props.converterReducer.CAD}
                        </div>
                        <div className='element'>
                            <img
                                alt='eur pic'
                                className='imgStyle'
                                width='50px'
                                src='/img/eur.jpg'
                            />
                            EUR: {props.converterReducer.EUR} = {props.converterReducer.RUB * props.converterReducer.EUR}
                        </div>
                        <div className='element'>
                            <img
                                alt='usd pic'
                                className='imgStyle'
                                width='50px'
                                src='/img/usa.jpg'
                            />
                            USD: {props.converterReducer.USD} = {props.converterReducer.RUB * props.converterReducer.USD}
                        </div>
                    </div>


            </BodyWrapper>

        </div>
    );
}

export default Body;
