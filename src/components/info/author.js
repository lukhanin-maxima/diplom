import './author.css';

function Author() {



    return (
        <div className="container">
          <img src='/img/me.jpg' alt="me" />
          <h1 >Денис Луханин</h1>
          <p>
            Автор проекта
          </p>
          <div className='link'>
            <a
            href="https://t.me/dzopstings">
            <img src="/img/telegram.png" width='100px' alt="Telegram"></img>     
          </a>
          </div>
            
          </div>
    );
}

export default Author;
