import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import converterReducer from "../reducers/consverter.js";

let reducersBatch = combineReducers({
    converterReducer: converterReducer
}) ;

const store = createStore(reducersBatch, applyMiddleware(thunkMiddleware));

export default store;